import React, { FC } from 'react'
import { Link, useLocation } from 'react-router-dom';
import { useActions } from '../../hooks/useActions';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { IThemes } from '../../models/ITheme';

import './Header.scss'

const Header: FC = () => {

  const { setPrompt, setTheme, logout } = useActions()
  const { isAuth, user } = useTypedSelector(state => state.auth);

  const showDrop = () => {
    document.body.classList.toggle("_menu")
    document.querySelector(".header__drop")!.classList.toggle("_active")
    document.querySelector(".header__burger")!.classList.toggle("_active")
  }

  const showProfile = () => {
    document.querySelector(".profile-header__drop")!.classList.toggle("_active")
    document.addEventListener("click", closeIfClicked, false)
  }

  const closeIfClicked = (event: any) => {
    if (event.target.closest(".profile-header__drop") == null && !(event.target.classList.contains("profile-header__avatar"))) {
      document.querySelector(".profile-header__drop")!.classList.remove("_active")
      document.body.classList.remove("_menu")
      document.removeEventListener("click", closeIfClicked, false)
    }
  }

  const changeTheme = () => {
    setTheme(IThemes.DARK);
    localStorage.setItem('theme', IThemes.DARK);
  };

  return (
    <header className='header'>
      <div className='header__inner'>
        <div className='header__logo logo-header'>
          <Link to='/' className='logo-header__logo' onClick={(e: any) => showDrop()}>
            <img className='header__logo-logo' src='https://cloud.wanilla.ru/api/static/home/reedus0/Hakathon/logo.png' />
          </Link>
        </div>
        <div className='header__drop'>
          <div className='header__links links-header'>
            <Link to='/complex' className='links-header__link' onClick={(e: any) => showDrop()}>Комплексы</Link>
            <Link to='/reviews' className='links-header__link' onClick={(e: any) => showDrop()}>Отзывы</Link>
            <Link to='/contacts' className='links-header__link' onClick={(e: any) => showDrop()}>Контакты</Link>
          </div>
          {isAuth &&
            <div className='header__profile profile-header'>
              <>
                <button className='profile-header__avatar-button'>
                  <img className='profile-header__avatar' onClick={() => showProfile()} width={35} height={35} src={"https://cloud.wanilla.ru/api/static/home/reedus0/Hakathon/avatar.png"} />
                </button>
                <div className='profile-header__drop'>
                  <div className='profile-header__info'>
                    <Link className='profile-header__avatar-link' to={'/users/'}><img className='profile-header__avatar' width={60} height={60} src={"https://cloud.wanilla.ru/api/static/home/reedus0/Hakathon/avatar.png"} /></Link>
                    <div className='profile-header__info-text'>
                      <h1 className='profile-header__name'>{user['name']}</h1>
                    </div>
                  </div>
                  <div className='profile-header__links'>
                    <Link to='/admin' className='profile-header__link-drop' onClick={(e: any) => showDrop()}>Админ панель</Link>
                    <a className='profile-header__link-drop' onClick={() => {showDrop(); logout()}}>Выйти</a>
                  </div>
                </div>
              </>

            </div>
          }
        </div>
        <label className="header__burger burger-header" onClick={() => showDrop()}>
          <span className="burger__toggle"></span>
        </label>
      </div>
    </header>
  )
}

export default Header

function useEffect(arg0: () => void, arg1: any[]) {
  throw new Error('Function not implemented.');
}
