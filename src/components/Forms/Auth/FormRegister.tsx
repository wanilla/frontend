import React, { useState } from 'react'
import { useActions } from '../../../hooks/useActions';
import { useTypedSelector } from '../../../hooks/useTypedSelector';
import { isNumeric } from '../../../Utils';
import Input from '../../Input/Input'
import ButtonPrompt from '../../Prompt/ButtonPrompt/ButtonPrompt'
import Prompt from '../../Prompt/Prompt';
import FormLogin from './FormLogin';



export default function FormRegister() {

  const { register, setPrompt, setAuthError } = useActions()
  const { authError, userIsLoading } = useTypedSelector(state => state.auth);

  const [name, nameSet] = useState<string>("")
  const [passwordPrimary, passwordPrimarySet] = useState<string>("")
  const [passwordSecondary, passwordSecondarySet] = useState<string>("")

  const [error, errorSet] = useState<string>("")


  function registerRequest(event: any) {
    event.preventDefault()
    if (passwordPrimary !== passwordSecondary) {
      errorSet('Пароли не совподают!')
      return
    }
    if (passwordPrimary.length < 8) {
      errorSet('Пароль должен содержать 8 символов')
      return
    }
    if(!isNumeric(name)){
      setAuthError('Введите настоящий ОГРН!')
      return
    }
    event.preventDefault()
    register(name, passwordPrimary)
  }

  return (
    <form className='form-auth' id="form-login" onSubmit={(e: any) => registerRequest(e)}>
      <Input value={name} onChange={(e: any) => nameSet(e.target.value)} name="name" type="text" placeholder="ОГРН" autoComplete='off' />
      <Input value={passwordPrimary} onChange={(e: any) => passwordPrimarySet(e.target.value)} name="passwordFirst" type="password" placeholder="Пароль" autoComplete='off' />
      <Input value={passwordSecondary} onChange={(e: any) => passwordSecondarySet(e.target.value)} name="passwordSecond" type="password" placeholder="Пароль снова" autoComplete='off' />
      <h1 className='form-auth__error'>{authError}</h1>
      <div className='buttons-prompt'>
        <ButtonPrompt name="Зарегистрироваться" function={(e: any) => registerRequest(e)} />
      </div>
      <h1 className='form-auth__link' onClick={() => setPrompt(
        <Prompt title="Вход">
          <FormLogin />
        </Prompt>
      )}
      >Вход</h1>
    </form>
  )
}
