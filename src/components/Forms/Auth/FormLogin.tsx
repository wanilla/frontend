import React, { useState } from 'react'
import { useActions } from '../../../hooks/useActions';
import { useTypedSelector } from '../../../hooks/useTypedSelector';
import Input from '../../Input/Input'
import ButtonPrompt from '../../Prompt/ButtonPrompt/ButtonPrompt'
import Prompt from '../../Prompt/Prompt';
import FormRegister from './FormRegister';

import './../Forms.scss'
import { isNumeric } from '../../../Utils';


export default function FormLogin() {


  const { login, setPrompt, setAuthError } = useActions()
  const { authError, userIsLoading } = useTypedSelector(state => state.auth);


  const [name, nameSet] = useState<string>("")
  const [password, passwordSet] = useState<string>("")

  function loginRequest(event: any) {
    event.preventDefault()
    if(!isNumeric(name)){
      setAuthError('Введите настоящий ОГРН!')
      return
    }
    login(name, password)
  }

  return (
    <form className='form-auth' id="form-login" onSubmit={(e: any) => loginRequest(e)}>
      <Input onChange={(e: any) => nameSet(e.target.value)} value={name} name="username" type="text" placeholder='ОГРН' autoComplete='off' />
      <Input onChange={(e: any) => passwordSet(e.target.value)} value={password} name="password" type="password" placeholder="Пароль" autoComplete='off' />
      <h1 className='form-auth__error'>{authError}</h1>
      <div className='buttons-prompt'>
        <ButtonPrompt name="Вход" function={(e: any) => loginRequest(e)} />
      </div>
      <h1 className='form-auth__link' onClick={() => setPrompt(
        <Prompt title="Регистрация">
          <FormRegister />
        </Prompt>
      )}
      >Регистрация</h1>
    </form>
  )
}
