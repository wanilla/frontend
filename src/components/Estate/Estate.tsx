import React from 'react'
import { Link } from 'react-router-dom'
import { IComplex } from '../../models/IComplex'

import './Estate.scss'

export default function Estate(props: { complex: IComplex }) {
  return (
    <Link to={'/complex/' + props.complex.id} className='estate'>
      <h1 className='estate__name'>{props.complex.name}</h1>
      <div className='estate__img-wrapper'>
        <img className='estate__img' src={props.complex.photo} />
      </div>
    </Link>
  )
}
