import React from 'react'
import { IComplex } from '../../models/IComplex'
import { IEstate } from '../../models/IEstate'
import { roundNumber } from '../../Utils'
import InfoField from '../InfoField/InfoField'

import './RatingItem.scss'

export default function RatingItem(props: { estate: IEstate, place: number, name: string }) {
    return (
        <div className='rating-item'>

            <>
                <h1 className='rating-item__place'>{props.place}</h1>
                <div className='rating-item__info'>
                    <h1 className='rating-item__complex'>{props.name}</h1>

                    <InfoField name='Площадь' value={props.estate.area.toString() + ' кв.м'} />
                    <InfoField name='Комнаты' value={props.estate.rooms.toString()} />
                    <InfoField name='Этаж' value={props.estate.max_floor.toString()} />
                    <InfoField name='Стоимость за кв.м' value={props.estate.cost.toLocaleString() + ' ₽'} />
                    <InfoField name='Стоимость аренды' value={Math.round(props.estate.rate!).toLocaleString() + ' ₽'} />
                    <InfoField name='Срок окупаемости' value={roundNumber(props.estate.years!) + ' лет'} />
                    {props.estate.sum && <InfoField name='Итоговый вклад' value={roundNumber(props.estate.sum!) + ' ₽'} />}

                </div>
            </>

        </div>
    )
}
