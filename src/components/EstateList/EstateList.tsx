import React from 'react'
import { useNavigate } from 'react-router-dom'
import { IComplex } from '../../models/IComplex'
import Button from '../Button/Button'
import Estate from '../Estate/Estate'

import './EstateList.scss'

export default function EstateList(props: { complexes: IComplex[], limit: number }) {

  const navigate = useNavigate()
  console.log(props.limit)
  return (
    <div className='estate-list'>
      {props.complexes.map((complex: IComplex, index: number) =>
        index <= props.limit && <Estate complex={complex} />
      )}
    </div>
  )
}
