import React from 'react'

import './InfoField.scss'

export default function InfoField(props: { name: string, value: string }) {
  return (
    <div className='info-field'>
        <h1 className='info-field__name'>{props.name}:</h1>
        <h2 className='info-field__value'>{props.value}</h2>
    </div>
  )
}
