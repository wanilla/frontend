import React from 'react'
import { IComplex } from '../../../models/IComplex'

import './Menu.scss'

export default function Menu(props: {complexes: IComplex[]}) {
    return (
        <div className='menu'>
            {props.complexes.map((estate: IComplex) => estate.addr)}
        </div>
    )
}
