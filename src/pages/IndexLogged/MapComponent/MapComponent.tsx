import React, { useState } from "react"
import { Map, Marker } from "pigeon-maps"
import { maptiler } from 'pigeon-maps/providers'

import './MapComponent.scss'
import { IComplex } from "../../../models/IComplex"

const maptilerProvider = maptiler('BX9a7Ntl0oUCcPQca5su', 'streets')


//45.0319377,38.9751638

export function MapComponent(props: { complexes: IComplex[] }) {

  const [lat, setLat] = useState(45.0319377)
  const [lon, setLon] = useState(38.9751638)


  return (
    <div className="map">
      <Map
        boxClassname="map"
        height={window.innerHeight - 60}
        width={window.innerWidth - ((window.innerWidth / 100) * 25)}
        defaultCenter={[+lat, +lon]}
        defaultZoom={16}
        center={[+lat, +lon]}
        provider={maptilerProvider}
      >
        {props.complexes.map((complex: IComplex) =>
          <Marker width={50} anchor={[Number(complex.lat), Number(complex.lon)]} />
        )}

      </Map>
    </div>

  )
}