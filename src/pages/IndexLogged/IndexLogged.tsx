import React, { useEffect, useState } from 'react'
import { MapComponent } from './MapComponent/MapComponent'
import Header from '../../components/Header/Header'
import './IndexLogged.scss'
import Menu from './Menu/Menu'
import { IEstate } from '../../models/IEstate'
import { EstatesService } from '../../api/EstatesService'
import { IComplex } from '../../models/IComplex'
import { ComplexService } from '../../api/ComplexService'

const IndexLogged = () => {

  const [complexes, complexesSet] = useState<IComplex[]>([])

  const setup = async () => {
    const complexesService = new ComplexService()
    const response = await complexesService.getAllComplex()
    console.log((await response.clone().json())['result'])
    complexesSet((await response.clone().json())['result'])
  }

  useEffect(() => {
    setup()
  }, [])


  return (
    <>
      <Header />
      <div className='logged'>
        <Menu
          complexes={complexes}
        />
        <MapComponent
          complexes={complexes}
        />
      </div>
    </>
  )
}

export default IndexLogged