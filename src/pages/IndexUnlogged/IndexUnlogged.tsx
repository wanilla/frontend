import React, { FC, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { ComplexService } from '../../api/ComplexService'
import Button from '../../components/Button/Button'
import EstateList from '../../components/EstateList/EstateList'
import { IComplex } from '../../models/IComplex'
import Intro from './components/Intro/Intro'
import { MapComponent } from './components/MapComponent/MapComponent'
import Mobile from './components/Mobile/Mobile'

import './IndexUnlogged.scss'

const IndexUnlogged: FC = () => {
  const [complexes, complexesSet] = useState<IComplex[]>([])
  const navigate = useNavigate()

  const setup = async () => {
    const complexesService = new ComplexService()
    const response = await complexesService.getAllComplex()
    console.log((await response.clone().json())['result'])
    complexesSet((await response.clone().json())['result'])
  }

  useEffect(() => {
    setup()
  }, [])

  return (
    <div className='unloged'>
      <Intro />
      <EstateList complexes={complexes} limit={7}/>
      <div className='estate-list__button-wrapper'>
        <Button name='Смотреть все комплексы' function={() => navigate('/complex')}/>
      </div>
      <MapComponent complexes={complexes}/>
      <Mobile />
    </div>
  )
}

export default IndexUnlogged