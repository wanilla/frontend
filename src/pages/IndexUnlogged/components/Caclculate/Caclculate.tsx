import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Input from '../../../../components/Input/Input'
import ButtonPrompt from '../../../../components/Prompt/ButtonPrompt/ButtonPrompt'
import { useActions } from '../../../../hooks/useActions'

export default function Caclculate() {

    const [sum, sumSet] = useState<number>()

    const { setPrompt } = useActions()

    const navigate = useNavigate()

    function calculateRequest(event: any) {
        event.preventDefault()
        navigate('/rating/calc/' + sum)
        setPrompt(<></>)
        
    }

    return (
        <form className='form-auth' id="form-login" onSubmit={(e: any) => calculateRequest(e)}>
            <Input onChange={(e: any) => sumSet(e.target.value)} value={sum} name="username" type="text" placeholder='Сумма вложений' autoComplete='off' />
            <div className='buttons-prompt'>
                <ButtonPrompt name="Рассчитать" function={(e: any) => calculateRequest(e)} />
            </div>
        </form>
    )
}
