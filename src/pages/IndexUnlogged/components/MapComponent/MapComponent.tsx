import React, { useState } from "react"
import { Map, Marker, Overlay } from "pigeon-maps"
import { maptiler } from 'pigeon-maps/providers'

import './MapComponent.scss'
import { IComplex } from "../../../../models/IComplex"
import { useNavigate } from "react-router-dom"

const maptilerProvider = maptiler('BX9a7Ntl0oUCcPQca5su', 'streets')


//45.0319377,38.9751638

export function MapComponent(props: { complexes: IComplex[] }) {

  const [lat, setLat] = useState(45.0319377)
  const [lon, setLon] = useState(38.9751638)

  const nabigate = useNavigate()

  return (
    <div className="map">
      <Map
        boxClassname="map"
        height={window.innerHeight - 200}
        width={window.innerWidth}
        defaultCenter={[lat, lon]}
        defaultZoom={16}
        center={[lat, lon]}
        provider={maptilerProvider}
      >
        {props.complexes.map((complex: IComplex) =>
          <Marker width={70} anchor={[+complex.lat, +complex.lon]} onClick={() => nabigate('/complex/' + complex.id)}/>
        )}

      </Map>
    </div>

  )
}