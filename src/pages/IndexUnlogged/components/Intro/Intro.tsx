import React from 'react'
import Button from '../../../../components/Button/Button'
import FormRegister from '../../../../components/Forms/Auth/FormRegister'
import Prompt from '../../../../components/Prompt/Prompt'
import { useActions } from '../../../../hooks/useActions'
import { useTypedSelector } from '../../../../hooks/useTypedSelector'
import Caclculate from '../Caclculate/Caclculate'
import Header from '../Header/Header'

import './Intro.scss'

export default function Intro() {

    const { setPrompt } = useActions()
    const { isAuth } = useTypedSelector(state => state.auth);

    return (
        <div className='intro'>
            <div className='intro__wrapper'>

                <Header />
                <div className='intro__inner'>
                    <div className='intro__container'>


                        <h1 className='intro__title'>
                            <p>Начните инвестрировать в <span>жилую</span> и</p>
                            <p>И <span>коммерческую</span> недвижимость</p>
                            <p>уже сегодня.</p>
                        </h1>
                        <h2 className='intro__subtitle'>
                            <p>Наш сервис поможет Вам грамотно инвестировать</p>
                            <p>Нв жилую или коммерческую недвижимость, рассчитать прибыль</p>
                            <p>Ни обойти риски.</p>
                        </h2>
                        <div className='intro__button-wrapper'>
                            <Button class='intro__button' name='НАЧАТЬ ИНВЕСТИРОВАТЬ'
                                function={() =>
                                    setPrompt(
                                        <Prompt title='Рассчитать'>
                                            <Caclculate />
                                        </Prompt>
                                    )}
                            />
                            {!isAuth &&
                                <Button class='intro__button _white _litle' name='Я застройщик' function={() => {
                                    setPrompt(
                                        <Prompt title='Регистрация'>
                                            <FormRegister />
                                        </Prompt>
                                    )
                                }} />
                            }
                        </div>
                    </div>
                </div>
            </div>

            <img src='https://cloud.wanilla.ru/api/static/home/reedus0/Hakathon/intro.png' className='intro__bg' />
        </div>
    )
}
