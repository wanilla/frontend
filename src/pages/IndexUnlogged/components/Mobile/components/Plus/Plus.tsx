import React from 'react'

import './Plus.scss'

export default function Plus(props: {name: string}) {
  return (
    <div className='plus'>
        <img className='plus__icon' src='https://cloud.wanilla.ru/api/static/home/reedus0/Hakathon/plus.png'/>
        <h1 className='plus__name'>{props.name}</h1>
    </div>
  )
}
