import React from 'react'
import Button from '../../../../components/Button/Button'
import Plus from './components/Plus/Plus'

import './Mobile.scss'

export default function Mobile() {
    return (
        <div className='mobile-download'>
            <div className='mobile-download__left'>
                <div className='mobile-download__img-wrapper'>
                    <img className='mobile-download__img' src='https://cloud.wanilla.ru/api/static/home/reedus0/Hakathon/phones.png' />

                </div>
            </div>
            <div className='mobile-download__right'>
                <h1 className='mobile-download__title'>
                    Инвестировать легче
                    в нашем мобильном
                    приложении
                </h1>
                <div className='mobile-download__pluses'>
                    <Plus name='Удобно в использовании' />
                    <Plus name='Постоянный контроль акций' />

                    <Plus name='Огромная карта' />

                </div>
                <div className='mobile-download__buttons'>
                    <Button name='Скачать для Android' class='mobile-download__button' />
                    <Button name='Скачать для IOS'  class='mobile-download__button'/>

                </div>
            </div>
        </div>
    )
}
