import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { ComplexService } from '../../../../api/ComplexService'
import Button from '../../../../components/Button/Button'
import { IComplex } from '../../../../models/IComplex'
import InfoField from '../../../../components/InfoField/InfoField'

import './ComplexInfo.scss'

export default function ComplexInfo(props: { complex: IComplex }) {
    const complexService = new ComplexService()

    const navigate = useNavigate()

    return (
        <div className='complex-info'>

            {props.complex ? <>
                <h1 className='complex-info__name'>{props.complex.name}</h1>
                <div className='complex-info__img-wrapper'>
                    <img className='complex-info__img' src={props.complex.photo} alt='https://cloud.wanilla.ru/api/static/home/reedus0/Hakathon/complex.jpg'/>
                </div>
                <div className='complex-info__rating'>
                    <img className='complex-info__star' src='https://cloud.wanilla.ru/api/static/home/reedus0/Hakathon/star.png'/>
                    <h1 className='complex-info__rating-rating'>Рейтинг ЖК: {props.complex.rating} из 100</h1>
                </div>

                <div className='complex-info'>
                    <InfoField name='Адрес' value={props.complex.addr} />
                    <Button name='РАССЧИТАТЬ СТОИМОСТЬ' class='complex-info__button' function={() => navigate('/rating/' + props.complex.id)} />

                </div>

            </> : <></>}

        </div>
    )
}
