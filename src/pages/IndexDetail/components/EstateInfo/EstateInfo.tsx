import React, { useEffect, useState } from 'react'
import Button from '../../../../components/Button/Button'
import InfoField from '../../../../components/InfoField/InfoField'
import { Map, Marker } from "pigeon-maps"
import { maptiler } from 'pigeon-maps/providers'


import './EstateInfo.scss'
import { IComplex } from '../../../../models/IComplex'

export default function EstateInfo(props: { complex: IComplex }) {

    const maptilerProvider = maptiler('BX9a7Ntl0oUCcPQca5su', 'streets')

    const [lat, setLat] = useState(45.0319377)
    const [lon, setLon] = useState(38.9751638)

    const setup = async () => {
        setLat(props.complex.lat)
        setLon(props.complex.lon)
    }

    useEffect(() => {
        setup()
    }, [props.complex])


    return (
        <div className='estate-info'>
            <div className='estate-info__map'>
                <Map
                    boxClassname="map"
                    height={800}
                    width={window.innerWidth > 991 ? window.innerWidth - ((window.innerWidth / 100) * 52) : window.innerWidth - 50}
                    defaultZoom={16}
                    center={[+lat, +lon]}
                    provider={maptilerProvider}
                >
                    <Marker width={50} anchor={[+lat, +lon]} />
                </Map>
            </div>

        </div>
    )
}
