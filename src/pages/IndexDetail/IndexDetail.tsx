import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { ComplexService } from '../../api/ComplexService'
import Header from '../../components/Header/Header'
import { IComplex } from '../../models/IComplex'
import ComplexInfo from './components/ComplexInfo/ComplexInfo'
import EstateInfo from './components/EstateInfo/EstateInfo'

import './IndexDetail.scss'

export default function IndexDetail() {

    const { id } = useParams()
    const [complex, complexSet] = useState<IComplex>({} as IComplex)

    const setup = async () => {
        const complexesService = new ComplexService()
        const response = await complexesService.getComplex(id)
        console.log((await response.clone().json()))
        complexSet((await response.clone().json()))
    }

    useEffect(() => {
        setup()

    }, [])



    return (
        <>
            <Header />
            <div className='detail'>
                <ComplexInfo
                    complex={complex}
                />

                <EstateInfo
                    complex={complex}
                />
            </div>
        </>
    )
}
