import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { ComplexService } from '../../api/ComplexService'
import Header from '../../components/Header/Header'
import { IComplex } from '../../models/IComplex'
import { IEstate } from '../../models/IEstate'
import RatingItem from '../../components/RatingItem/RatingItem'

import './RatingSum.scss'

export default function RatingSum() {


    const { sum } = useParams()
    const [complex, complexSet] = useState<IComplex>({} as IComplex)
    const [estates, estatesSet] = useState<IEstate[]>([])

    const [error, errorSet] = useState<string>('')


    const setup = async () => {
        const complexesService = new ComplexService()
        const complexResponse = await complexesService.calculateAll(Number(sum))
        console.log(complexResponse.status)
        if (complexResponse.status === 200) {
            estatesSet((await complexResponse.clone().json())['result'])
        } else {
            errorSet((await complexResponse.clone().json())['error'])
        }
    }

    useEffect(() => {
        setup()
    }, [])

    return (
        <>
            <Header />
            <h1 className='rating__title'>Мы подобрали для Вас лучшие решения:</h1>
            {!error ?
                <div className='rating'>
                    {estates.map((estate: IEstate, index: number) => index < 4 && <RatingItem name={estate.rc_name!} estate={estate} place={index + 1} />)}
                </div>
                : <h1 className='rating__error'>{error}</h1>
            }
        </>
    )
}
