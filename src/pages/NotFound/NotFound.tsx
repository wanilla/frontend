import React from 'react'
import { useNavigate } from 'react-router-dom'
import Button from '../../components/Button/Button'
import Header from '../../components/Header/Header'

import './NotFound.scss'

export default function NotFound() {


    const navigate = useNavigate()

    return (
        <>
            <Header />
            <div className='not-found'>
                <div className='not-found__inner' >
                    <h1 className='not-found__bg'>404</h1>
                    <h1 className='not-found__title'>Страница не найдена</h1>
                    <Button name='На главную' class='not-found__button' function={() => navigate('/')} />
                </div>
            </div>
        </>
    )
}
