import React, { useEffect, useState } from 'react'
import { ComplexService } from '../../api/ComplexService'
import Button from '../../components/Button/Button'
import Header from '../../components/Header/Header'
import Prompt from '../../components/Prompt/Prompt'
import { useActions } from '../../hooks/useActions'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { IComplex } from '../../models/IComplex'

import './Admin.scss'
import AddComplex from './components/AddComplex/AddComplex'
import AdminItem from './components/AdminItem/AdminItem'

export default function Admin() {

    const { setPrompt } = useActions()

    const [complexes, complexesSet] = useState<IComplex[]>([])
    const { user } = useTypedSelector(state => state.auth);


    const setup = async () => {
        const complexesService = new ComplexService()
        const response = await complexesService.getAllComplex()
        console.log((await response.clone().json())['result'])
        complexesSet((await response.clone().json())['result'].filter((complex: IComplex) => complex.builder === user.id))
    }

    useEffect(() => {
        setup()
    }, [])

    return (
        <>
            <Header />
            <div className='admin'>
                <h1 className='admin__title'>Мои комплексы</h1>
                {complexes.map((complex: IComplex) => <AdminItem complex={complex} />)}
                <Button class='admin__add' name='Добавить комплекс' function={() => {
                    setPrompt(
                        <Prompt title='Добавить комплекс' >
                            <AddComplex complexes={complexes} complexesSet={complexesSet} />
                        </Prompt>)
                }} />
            </div>
        </>
    )
}
