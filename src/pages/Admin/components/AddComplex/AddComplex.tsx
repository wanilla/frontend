import React, { useState } from 'react'
import { ComplexService } from '../../../../api/ComplexService';
import Input from '../../../../components/Input/Input'
import ButtonPrompt from '../../../../components/Prompt/ButtonPrompt/ButtonPrompt'
import { useActions } from '../../../../hooks/useActions';
import { useTypedSelector } from '../../../../hooks/useTypedSelector';
import { IComplex } from '../../../../models/IComplex';
import { isNumeric } from '../../../../Utils';

export default function AddComplex(props: { complexes: IComplex[], complexesSet: Function }) {

    const { setPrompt } = useActions()
    const [form, formSet] = useState<any>({})
    const { user } = useTypedSelector(state => state.auth);

    const [error, errorSet] = useState<string>('')


    const addComplexRequest = async (event: any) => {
        event.preventDefault()
        if(!isNumeric(form['lat']) || !isNumeric('lon')|| !isNumeric('rating')) {
            errorSet('Введены неверные данные')
            return 
        }
        form['builder'] = user['id']
        const compexService = new ComplexService()
        const response = await compexService.addComplex(form)
        if (response.status === 200) {
            props.complexesSet([...props.complexes, {
                ...form,
                id: (await (response.clone().json()))['result'],
            },
            ])
            setPrompt(<></>)
        }
    }

    return (
        <form className='form-auth' id="form-login" onSubmit={(e: any) => addComplexRequest(e)}>
            <Input onChange={(e: any) => form['name'] = e.target.value} value={form['name']} name="username" type="text" placeholder='Имя' autoComplete='off' />
            <Input onChange={(e: any) => form['addr'] = e.target.value} value={form['addr']} name="username" type="text" placeholder='Адрес' autoComplete='off' />
            <Input onChange={(e: any) => form['lat'] = e.target.value} value={form['lat']} name="username" type="text" placeholder='Широта' autoComplete='off' />
            <Input onChange={(e: any) => form['lon'] = e.target.value} value={form['lon']} name="username" type="text" placeholder='Долгота' autoComplete='off' />
            <Input onChange={(e: any) => form['rating'] = e.target.value} value={form['rating']} name="username" type="text" placeholder='Рейтинг' autoComplete='off' />
            <Input onChange={(e: any) => form['photo'] = e.target.value} value={form['photo']} name="username" type="text" placeholder='Ссылка на фото' autoComplete='off' />
            <h1 className='form-auth__error'>{error}</h1>
            <div className='buttons-prompt'>
                <ButtonPrompt name="Отправить" function={(e: any) => addComplexRequest(e)} />
            </div>
        </form>
    )
}
