import React, { useEffect, useState } from 'react'
import { EstatesService } from '../../../../api/EstatesService'
import Prompt from '../../../../components/Prompt/Prompt'
import { useActions } from '../../../../hooks/useActions'
import { IComplex } from '../../../../models/IComplex'
import { IEstate } from '../../../../models/IEstate'
import AddEstate from '../AddEstate/AddEstate'

import './AdminItem.scss'

export default function AdminItem(props: { complex: IComplex }) {

    const { setPrompt } = useActions()

    const [estates, estatesSet] = useState<IEstate[]>([])

    const setup = async () => {
        const estateService = new EstatesService()
        const response = await estateService.getAllEstate()
        console.log(await (response.clone().json()))
        console.log(props.complex.id)
        estatesSet((await (response.clone().json()))['result'].filter((estate: IEstate) => estate.rcomplex == props.complex.id))
    }

    useEffect(() => {
        setup()
    }, [])

    return (
        <div className='admin-item'>
            <div className='admin-item__inner'>
                <h1 className='admin-item__field'>{props.complex.name}</h1>
                <h2 className='admin-item__field'>{props.complex.addr}</h2>
                <h2 className='admin-item__field'>{props.complex.rating}</h2>
            </div>
            {estates.map((estate: IEstate) =>
                <div className='admin-item__estate estate-admin'>
                    <h1 className='admin-item__field'>{estate.rcomplex}</h1>
                    <h1 className='admin-item__field'>{estate.rate}</h1>
                    <h1 className='admin-item__field'>{estate.rooms}</h1>

                </div>
            )}

            <div className='admin-item__estate estate-admin'>
                <h1 className='estate-admin__add' onClick={() => {
                    setPrompt(
                        <Prompt title='Добавить квартиру' >
                            <AddEstate complex={props.complex} estates={estates} estatesSet={estatesSet} />
                        </Prompt>)
                }} >Добавить квартиру</h1>
            </div>
        </div>
    )
}
