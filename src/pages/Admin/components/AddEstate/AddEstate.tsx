import React, { useState } from 'react'
import { ComplexService } from '../../../../api/ComplexService';
import { EstatesService } from '../../../../api/EstatesService';
import Input from '../../../../components/Input/Input';
import ButtonPrompt from '../../../../components/Prompt/ButtonPrompt/ButtonPrompt';
import { useActions } from '../../../../hooks/useActions';
import { useTypedSelector } from '../../../../hooks/useTypedSelector';
import { IComplex } from '../../../../models/IComplex';
import { IEstate } from '../../../../models/IEstate';
import { isNumeric } from '../../../../Utils';

export default function AddEstate(props: { complex: IComplex, estates: IEstate[], estatesSet: Function }) {

    const { setPrompt } = useActions()
    const [form, formSet] = useState<any>({})

    const [error, errorSet] = useState<string>('')


    const addEstateRequest = async (event: any) => {
        event.preventDefault()
        if(!isNumeric(form['area']) || !isNumeric(form['max_floor']) || !isNumeric(form['rooms']) || !isNumeric(form['cost'])) {

            return
        }
        form['rcomplex'] = props.complex.id
        const estateService = new EstatesService()
        const response = await estateService.addEstate(form)
        if (response.status === 200) {
            props.estatesSet([...props.estates, {
                ...form,
                id: (await (response.clone().json()))['result'],
            }])
            setPrompt(<></>)
        }
    }

    return (
        <form className='form-auth' id="form-login" onSubmit={(e: any) => addEstateRequest(e)}>
            <Input onChange={(e: any) => form['area'] = e.target.value} value={form['area']} name="username" type="text" placeholder='Площадь, кв.м' autoComplete='off' />
            <Input onChange={(e: any) => form['max_floor'] = e.target.value} value={form['max_floor']} name="username" type="text" placeholder='Этаж' autoComplete='off' />
            <Input onChange={(e: any) => form['rooms'] = e.target.value} value={form['rooms']} name="username" type="text" placeholder='Количетсво комнат' autoComplete='off' />
            <Input onChange={(e: any) => form['cost'] = e.target.value} value={form['cost']} name="username" type="text" placeholder='Цена за кв.м' autoComplete='off' />
            <h1 className='form-auth__error'>{error}</h1>
            <div className='buttons-prompt'>
                <ButtonPrompt name="Отправить" function={(e: any) => addEstateRequest(e)} />
            </div>
        </form>

    )
}
