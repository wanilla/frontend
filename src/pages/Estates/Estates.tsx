import React, { useEffect, useState } from 'react'
import { ComplexService } from '../../api/ComplexService'
import EstateList from '../../components/EstateList/EstateList'
import Header from '../../components/Header/Header'
import { IComplex } from '../../models/IComplex'

export default function Estates() {

    const [complexes, complexesSet] = useState<IComplex[]>([])

    const setup = async () => {
        const complexesService = new ComplexService()
        const response = await complexesService.getAllComplex()
        console.log((await response.clone().json())['result'])
        complexesSet((await response.clone().json())['result'])
    }

    useEffect(() => {
        setup()
    }, [])

    return (
        <div className='estates-page'>
            <Header />
            <EstateList complexes={complexes} limit={99999}/>
        </div>
    )
}
