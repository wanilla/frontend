export interface IEstate {
    area: number;
    cost: number;
    id: number;
    max_floor: number;
    rcomplex: number;
    rooms: number;
    rate?: number;
    years?: number;
    rc_name? : string;
    sum?: number;
}