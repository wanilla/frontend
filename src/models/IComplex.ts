export interface IComplex {
    id: number;
    name: string;
    photo: string;
    builder: number;
    rating: number;
    lat: number;
    lon: number;
    addr: string;
}