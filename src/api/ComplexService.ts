
import { API } from "./API";

export class ComplexService {
  API = new API()
  getAllComplex() {
    return this.API.postRequest('/api/rcomplex/all')
  }
  getComplex(id: any) {
    return this.API.postRequest('/api/rcomplex/' + id)
  }
  addComplex(data: any) {
    return this.API.postRequest('/api/rcomplex/add', data = data)
  }
  calculate(id: any, rating: number) {
    return this.API.postRequest('/api/calc', { 'id': id, 'rating': rating })
  }
  calculateAll(sum: number) {
    return this.API.postRequest('/api/calc/all', { 'sum': sum })
  }
}