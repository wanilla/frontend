
import { API } from "./API";

export class UserService {
  API = new API()
  login(ogrn: string, password: string) {
    return this.API.postRequest('/api/oauth', { 'ogrn': ogrn, 'password': password })
  }
  register(ogrn: string, password: string) {
    return this.API.postRequest('/api/oregister', {'password': password, 'ogrn': ogrn})
  }
  getUser() {
    return this.API.postRequest('/api/whoami')
  }
}