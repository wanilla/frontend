
import { API } from "./API";

export class EstatesService {
  API = new API()
  getAllEstate() {
    return this.API.postRequest('/api/estate/all')
  }
  addEstate(data: any) {
    return this.API.postRequest('/api/estate/add', data)
  }
  getEstates(id: string) {
    return this.API.postRequest('/api/estate/get', { 'id': id })
  }
}