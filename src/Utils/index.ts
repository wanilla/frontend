export const roundNumber = (number: number) => {
    return (Math.round(number * 10) / 10).toLocaleString()
}

export const isNumeric = (str: string) => {
    if (typeof str != "string") return false  
    return !isNaN(str as any) && 
        !isNaN(parseFloat(str))
}