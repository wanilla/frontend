import React from 'react';
import Admin from '../pages/Admin/Admin';
import Estates from '../pages/Estates/Estates';
import IndexDetail from '../pages/IndexDetail/IndexDetail';
import IndexLogged from '../pages/IndexLogged/IndexLogged';
import IndexUnlogged from '../pages/IndexUnlogged/IndexUnlogged';
import Rating from '../pages/Rating/Rating';
import RatingSum from '../pages/RatingSum/RatingSum';


export interface IRoute {
  path: string;
  element: React.ReactNode;
}

export const privateRoutes: IRoute[] = [
  { path: "/", element: <IndexUnlogged /> },
  { path: "/admin", element: <Admin /> },
  { path: "/complex/:id", element: <IndexDetail /> },
  { path: "/rating/:id", element: <Rating /> },
  { path: "/rating/calc/:sum", element: <RatingSum /> },

  { path: "/complex", element: <Estates /> }

]

export const publicRoutes: IRoute[] = [
  { path: "/", element: <IndexUnlogged /> },
  { path: "/complex/:id", element: <IndexDetail /> },
  { path: "/rating/:id", element: <Rating /> },
  { path: "/rating/calc/:sum", element: <RatingSum /> },
  { path: "/complex", element: <Estates /> }

]


